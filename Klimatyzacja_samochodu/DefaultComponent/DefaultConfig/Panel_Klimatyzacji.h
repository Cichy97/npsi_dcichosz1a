/*********************************************************************
	Rhapsody	: 8.3.1 
	Login		: student
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: Panel_Klimatyzacji
//!	Generated Date	: Mon, 31, Aug 2020  
	File Path	: DefaultComponent/DefaultConfig/Panel_Klimatyzacji.h
*********************************************************************/

#ifndef Panel_Klimatyzacji_H
#define Panel_Klimatyzacji_H

//## auto_generated
#include <oxf/oxf.h>
//## auto_generated
#include <aom/aom.h>
//## auto_generated
#include "Default.h"
//## class Panel_Klimatyzacji
#include "Modul.h"
//## auto_generated
#include <oxf/omthread.h>
//## auto_generated
#include <oxf/omreactive.h>
//## auto_generated
#include <oxf/state.h>
//## auto_generated
#include <oxf/event.h>
//## link itsSterownik
class Sterownik;

//## package Default

//## class Panel_Klimatyzacji
class Panel_Klimatyzacji : public Modul {
public :

    //## auto_generated
    class Przycisk_Nadmuch_Na_Nogi;
    
    //## auto_generated
    class Przycisk_Nadmuch_Na_Szybe;
    
    //## auto_generated
    class Przycisk_Nadmuch_Srodkowy;
    
    //## auto_generated
    class Przycisk_Obieg_Wewnetrzny;
    
    //## auto_generated
    class Przycisk_Obieg_Zewnetrzny;
    
    //## auto_generated
    class Przycisk_Wlacz;
    
    //## auto_generated
    class Przycisk_Wylacz;
    
    //## auto_generated
    class Przycisk_Zmniejsz_Nawiew;
    
    //## auto_generated
    class Przycisk_Zmniejsz_Temp;
    
    //## auto_generated
    class Przycisk_Zwieksz_Nawiew;
    
    //## auto_generated
    class Przycisk_Zwieksz_Temp;
    
    //## class Panel_Klimatyzacji::Przycisk_Zwieksz_Temp
    class Przycisk_Zwieksz_Temp {
        ////    Friends    ////
        
    public :
    
    #ifdef _OMINSTRUMENT
        friend class OMAnimatedPrzycisk_Zwieksz_Temp;
    #endif // _OMINSTRUMENT
    
        ////    Constructors and destructors    ////
        
        //## auto_generated
        Przycisk_Zwieksz_Temp();
        
        //## auto_generated
        ~Przycisk_Zwieksz_Temp();
    };
    
    //## class Panel_Klimatyzacji::Przycisk_Zwieksz_Nawiew
    class Przycisk_Zwieksz_Nawiew {
        ////    Friends    ////
        
    public :
    
    #ifdef _OMINSTRUMENT
        friend class OMAnimatedPrzycisk_Zwieksz_Nawiew;
    #endif // _OMINSTRUMENT
    
        ////    Constructors and destructors    ////
        
        //## auto_generated
        Przycisk_Zwieksz_Nawiew();
        
        //## auto_generated
        ~Przycisk_Zwieksz_Nawiew();
    };
    
    //## class Panel_Klimatyzacji::Przycisk_Zmniejsz_Temp
    class Przycisk_Zmniejsz_Temp {
        ////    Friends    ////
        
    public :
    
    #ifdef _OMINSTRUMENT
        friend class OMAnimatedPrzycisk_Zmniejsz_Temp;
    #endif // _OMINSTRUMENT
    
        ////    Constructors and destructors    ////
        
        //## auto_generated
        Przycisk_Zmniejsz_Temp();
        
        //## auto_generated
        ~Przycisk_Zmniejsz_Temp();
    };
    
    //## class Panel_Klimatyzacji::Przycisk_Zmniejsz_Nawiew
    class Przycisk_Zmniejsz_Nawiew {
        ////    Friends    ////
        
    public :
    
    #ifdef _OMINSTRUMENT
        friend class OMAnimatedPrzycisk_Zmniejsz_Nawiew;
    #endif // _OMINSTRUMENT
    
        ////    Constructors and destructors    ////
        
        //## auto_generated
        Przycisk_Zmniejsz_Nawiew();
        
        //## auto_generated
        ~Przycisk_Zmniejsz_Nawiew();
    };
    
    //## class Panel_Klimatyzacji::Przycisk_Wylacz
    class Przycisk_Wylacz : public OMReactive {
        ////    Friends    ////
        
    public :
    
    #ifdef _OMINSTRUMENT
        friend class OMAnimatedPrzycisk_Wylacz;
    #endif // _OMINSTRUMENT
    
        ////    Constructors and destructors    ////
        
        //## auto_generated
        Przycisk_Wylacz(IOxfActive* theActiveContext = 0);
        
        //## auto_generated
        ~Przycisk_Wylacz();
        
        ////    Additional operations    ////
        
        //## auto_generated
        virtual bool startBehavior();
    };
    
    //## class Panel_Klimatyzacji::Przycisk_Wlacz
    class Przycisk_Wlacz : public OMReactive {
        ////    Friends    ////
        
    public :
    
    #ifdef _OMINSTRUMENT
        friend class OMAnimatedPrzycisk_Wlacz;
    #endif // _OMINSTRUMENT
    
        ////    Constructors and destructors    ////
        
        //## auto_generated
        Przycisk_Wlacz(IOxfActive* theActiveContext = 0);
        
        //## auto_generated
        ~Przycisk_Wlacz();
        
        ////    Additional operations    ////
        
        //## auto_generated
        virtual bool startBehavior();
    };
    
    //## class Panel_Klimatyzacji::Przycisk_Obieg_Zewnetrzny
    class Przycisk_Obieg_Zewnetrzny {
        ////    Friends    ////
        
    public :
    
    #ifdef _OMINSTRUMENT
        friend class OMAnimatedPrzycisk_Obieg_Zewnetrzny;
    #endif // _OMINSTRUMENT
    
        ////    Constructors and destructors    ////
        
        //## auto_generated
        Przycisk_Obieg_Zewnetrzny();
        
        //## auto_generated
        ~Przycisk_Obieg_Zewnetrzny();
    };
    
    //## class Panel_Klimatyzacji::Przycisk_Obieg_Wewnetrzny
    class Przycisk_Obieg_Wewnetrzny {
        ////    Friends    ////
        
    public :
    
    #ifdef _OMINSTRUMENT
        friend class OMAnimatedPrzycisk_Obieg_Wewnetrzny;
    #endif // _OMINSTRUMENT
    
        ////    Constructors and destructors    ////
        
        //## auto_generated
        Przycisk_Obieg_Wewnetrzny();
        
        //## auto_generated
        ~Przycisk_Obieg_Wewnetrzny();
    };
    
    //## class Panel_Klimatyzacji::Przycisk_Nadmuch_Srodkowy
    class Przycisk_Nadmuch_Srodkowy {
        ////    Friends    ////
        
    public :
    
    #ifdef _OMINSTRUMENT
        friend class OMAnimatedPrzycisk_Nadmuch_Srodkowy;
    #endif // _OMINSTRUMENT
    
        ////    Constructors and destructors    ////
        
        //## auto_generated
        Przycisk_Nadmuch_Srodkowy();
        
        //## auto_generated
        ~Przycisk_Nadmuch_Srodkowy();
    };
    
    //## class Panel_Klimatyzacji::Przycisk_Nadmuch_Na_Szybe
    class Przycisk_Nadmuch_Na_Szybe {
        ////    Friends    ////
        
    public :
    
    #ifdef _OMINSTRUMENT
        friend class OMAnimatedPrzycisk_Nadmuch_Na_Szybe;
    #endif // _OMINSTRUMENT
    
        ////    Constructors and destructors    ////
        
        //## auto_generated
        Przycisk_Nadmuch_Na_Szybe();
        
        //## auto_generated
        ~Przycisk_Nadmuch_Na_Szybe();
    };
    
    //## class Panel_Klimatyzacji::Przycisk_Nadmuch_Na_Nogi
    class Przycisk_Nadmuch_Na_Nogi {
        ////    Friends    ////
        
    public :
    
    #ifdef _OMINSTRUMENT
        friend class OMAnimatedPrzycisk_Nadmuch_Na_Nogi;
    #endif // _OMINSTRUMENT
    
        ////    Constructors and destructors    ////
        
        //## auto_generated
        Przycisk_Nadmuch_Na_Nogi();
        
        //## auto_generated
        ~Przycisk_Nadmuch_Na_Nogi();
    };
    
    ////    Friends    ////
    
#ifdef _OMINSTRUMENT
    friend class OMAnimatedPanel_Klimatyzacji;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    Panel_Klimatyzacji();
    
    //## auto_generated
    ~Panel_Klimatyzacji();
    
    ////    Additional operations    ////
    
    //## auto_generated
    Sterownik* getItsSterownik() const;
    
    //## auto_generated
    void setItsSterownik(Sterownik* p_Sterownik);

protected :

    //## auto_generated
    void cleanUpRelations();
    
    ////    Relations and components    ////
    
    Sterownik* itsSterownik;		//## link itsSterownik
    
    ////    Framework operations    ////

public :

    //## auto_generated
    void __setItsSterownik(Sterownik* p_Sterownik);
    
    //## auto_generated
    void _setItsSterownik(Sterownik* p_Sterownik);
    
    //## auto_generated
    void _clearItsSterownik();
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedPanel_Klimatyzacji : public OMAnimatedModul {
    DECLARE_META(Panel_Klimatyzacji, OMAnimatedPanel_Klimatyzacji)
    
    ////    Framework operations    ////
    
public :

    virtual void serializeAttributes(AOMSAttributes* aomsAttributes) const;
    
    virtual void serializeRelations(AOMSRelations* aomsRelations) const;
};

class OMAnimatedPrzycisk_Zwieksz_Temp : virtual public AOMInstance {
    DECLARE_META(Panel_Klimatyzacji::Przycisk_Zwieksz_Temp, OMAnimatedPrzycisk_Zwieksz_Temp)
};

class OMAnimatedPrzycisk_Zwieksz_Nawiew : virtual public AOMInstance {
    DECLARE_META(Panel_Klimatyzacji::Przycisk_Zwieksz_Nawiew, OMAnimatedPrzycisk_Zwieksz_Nawiew)
};

class OMAnimatedPrzycisk_Zmniejsz_Temp : virtual public AOMInstance {
    DECLARE_META(Panel_Klimatyzacji::Przycisk_Zmniejsz_Temp, OMAnimatedPrzycisk_Zmniejsz_Temp)
};

class OMAnimatedPrzycisk_Zmniejsz_Nawiew : virtual public AOMInstance {
    DECLARE_META(Panel_Klimatyzacji::Przycisk_Zmniejsz_Nawiew, OMAnimatedPrzycisk_Zmniejsz_Nawiew)
};

class OMAnimatedPrzycisk_Wylacz : virtual public AOMInstance {
    DECLARE_META(Panel_Klimatyzacji::Przycisk_Wylacz, OMAnimatedPrzycisk_Wylacz)
};

class OMAnimatedPrzycisk_Wlacz : virtual public AOMInstance {
    DECLARE_META(Panel_Klimatyzacji::Przycisk_Wlacz, OMAnimatedPrzycisk_Wlacz)
};

class OMAnimatedPrzycisk_Obieg_Zewnetrzny : virtual public AOMInstance {
    DECLARE_META(Panel_Klimatyzacji::Przycisk_Obieg_Zewnetrzny, OMAnimatedPrzycisk_Obieg_Zewnetrzny)
};

class OMAnimatedPrzycisk_Obieg_Wewnetrzny : virtual public AOMInstance {
    DECLARE_META(Panel_Klimatyzacji::Przycisk_Obieg_Wewnetrzny, OMAnimatedPrzycisk_Obieg_Wewnetrzny)
};

class OMAnimatedPrzycisk_Nadmuch_Srodkowy : virtual public AOMInstance {
    DECLARE_META(Panel_Klimatyzacji::Przycisk_Nadmuch_Srodkowy, OMAnimatedPrzycisk_Nadmuch_Srodkowy)
};

class OMAnimatedPrzycisk_Nadmuch_Na_Szybe : virtual public AOMInstance {
    DECLARE_META(Panel_Klimatyzacji::Przycisk_Nadmuch_Na_Szybe, OMAnimatedPrzycisk_Nadmuch_Na_Szybe)
};

class OMAnimatedPrzycisk_Nadmuch_Na_Nogi : virtual public AOMInstance {
    DECLARE_META(Panel_Klimatyzacji::Przycisk_Nadmuch_Na_Nogi, OMAnimatedPrzycisk_Nadmuch_Na_Nogi)
};
//#]
#endif // _OMINSTRUMENT

#endif
/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/Panel_Klimatyzacji.h
*********************************************************************/
