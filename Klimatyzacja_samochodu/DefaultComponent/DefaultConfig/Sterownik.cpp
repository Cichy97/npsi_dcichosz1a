/********************************************************************
	Rhapsody	: 8.3.1 
	Login		: student
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: Sterownik
//!	Generated Date	: Mon, 31, Aug 2020  
	File Path	: DefaultComponent/DefaultConfig/Sterownik.cpp
*********************************************************************/

//#[ ignore
#define NAMESPACE_PREFIX
//#]

//## auto_generated
#include "Sterownik.h"
//#[ ignore
#define Default_Sterownik_Sterownik_SERIALIZE OM_NO_OP
//#]

//## package Default

//## class Sterownik
Sterownik::Sterownik(IOxfActive* theActiveContext) {
    NOTIFY_REACTIVE_CONSTRUCTOR(Sterownik, Sterownik(), 0, Default_Sterownik_Sterownik_SERIALIZE);
    setActiveContext(theActiveContext, false);
    {
        {
            itsKlimatyzacja.setShouldDelete(false);
        }
        {
            itsWentylator.setShouldDelete(false);
        }
    }
    itsKlimatyzacja_1 = NULL;
    initRelations();
}

Sterownik::~Sterownik() {
    NOTIFY_DESTRUCTOR(~Sterownik, true);
    cleanUpRelations();
}

int Sterownik::getImpulsOK() const {
    return impulsOK;
}

void Sterownik::setImpulsOK(int p_impulsOK) {
    impulsOK = p_impulsOK;
}

int Sterownik::getStan() const {
    return stan;
}

void Sterownik::setStan(int p_stan) {
    stan = p_stan;
}

Klimatyzacja* Sterownik::getItsKlimatyzacja() const {
    return (Klimatyzacja*) &itsKlimatyzacja;
}

Klimatyzacja* Sterownik::getItsKlimatyzacja_1() const {
    return itsKlimatyzacja_1;
}

void Sterownik::setItsKlimatyzacja_1(Klimatyzacja* p_Klimatyzacja) {
    _setItsKlimatyzacja_1(p_Klimatyzacja);
}

Obieg_nagrzewnicy* Sterownik::getItsObieg_nagrzewnicy() const {
    return (Obieg_nagrzewnicy*) &itsObieg_nagrzewnicy;
}

Panel_Klimatyzacji* Sterownik::getItsPanel_Klimatyzacji() const {
    return (Panel_Klimatyzacji*) &itsPanel_Klimatyzacji;
}

Wentylator* Sterownik::getItsWentylator() const {
    return (Wentylator*) &itsWentylator;
}

bool Sterownik::startBehavior() {
    bool done = true;
    done &= itsKlimatyzacja.startBehavior();
    done &= itsWentylator.startBehavior();
    done &= OMReactive::startBehavior();
    return done;
}

void Sterownik::initRelations() {
    itsKlimatyzacja._setItsSterownik(this);
    itsObieg_nagrzewnicy._setItsSterownik(this);
    itsPanel_Klimatyzacji._setItsSterownik(this);
    itsWentylator._setItsSterownik(this);
}

void Sterownik::cleanUpRelations() {
    if(itsKlimatyzacja_1 != NULL)
        {
            NOTIFY_RELATION_CLEARED("itsKlimatyzacja_1");
            itsKlimatyzacja_1 = NULL;
        }
}

void Sterownik::__setItsKlimatyzacja_1(Klimatyzacja* p_Klimatyzacja) {
    itsKlimatyzacja_1 = p_Klimatyzacja;
    if(p_Klimatyzacja != NULL)
        {
            NOTIFY_RELATION_ITEM_ADDED("itsKlimatyzacja_1", p_Klimatyzacja, false, true);
        }
    else
        {
            NOTIFY_RELATION_CLEARED("itsKlimatyzacja_1");
        }
}

void Sterownik::_setItsKlimatyzacja_1(Klimatyzacja* p_Klimatyzacja) {
    __setItsKlimatyzacja_1(p_Klimatyzacja);
}

void Sterownik::_clearItsKlimatyzacja_1() {
    NOTIFY_RELATION_CLEARED("itsKlimatyzacja_1");
    itsKlimatyzacja_1 = NULL;
}

void Sterownik::setActiveContext(IOxfActive* theActiveContext, bool activeInstance) {
    OMReactive::setActiveContext(theActiveContext, activeInstance);
    {
        itsKlimatyzacja.setActiveContext(theActiveContext, false);
        itsWentylator.setActiveContext(theActiveContext, false);
    }
}

void Sterownik::destroy() {
    itsKlimatyzacja.destroy();
    itsWentylator.destroy();
    OMReactive::destroy();
}

#ifdef _OMINSTRUMENT
//#[ ignore
void OMAnimatedSterownik::serializeAttributes(AOMSAttributes* aomsAttributes) const {
    aomsAttributes->addAttribute("impulsOK", x2String(myReal->impulsOK));
    aomsAttributes->addAttribute("stan", x2String(myReal->stan));
}

void OMAnimatedSterownik::serializeRelations(AOMSRelations* aomsRelations) const {
    aomsRelations->addRelation("itsKlimatyzacja", true, true);
    aomsRelations->ADD_ITEM(&myReal->itsKlimatyzacja);
    aomsRelations->addRelation("itsKlimatyzacja_1", false, true);
    if(myReal->itsKlimatyzacja_1)
        {
            aomsRelations->ADD_ITEM(myReal->itsKlimatyzacja_1);
        }
    aomsRelations->addRelation("itsPanel_Klimatyzacji", true, true);
    aomsRelations->ADD_ITEM(&myReal->itsPanel_Klimatyzacji);
    aomsRelations->addRelation("itsWentylator", true, true);
    aomsRelations->ADD_ITEM(&myReal->itsWentylator);
    aomsRelations->addRelation("itsObieg_nagrzewnicy", true, true);
    aomsRelations->ADD_ITEM(&myReal->itsObieg_nagrzewnicy);
}
//#]

IMPLEMENT_REACTIVE_META_SIMPLE_P(Sterownik, Default, Default, false, OMAnimatedSterownik)
#endif // _OMINSTRUMENT

/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/Sterownik.cpp
*********************************************************************/
