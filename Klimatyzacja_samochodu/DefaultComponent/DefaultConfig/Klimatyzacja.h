/*********************************************************************
	Rhapsody	: 8.3.1 
	Login		: student
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: Klimatyzacja
//!	Generated Date	: Mon, 31, Aug 2020  
	File Path	: DefaultComponent/DefaultConfig/Klimatyzacja.h
*********************************************************************/

#ifndef Klimatyzacja_H
#define Klimatyzacja_H

//## auto_generated
#include <oxf/oxf.h>
//## auto_generated
#include <aom/aom.h>
//## auto_generated
#include "Default.h"
//## auto_generated
#include <oxf/omthread.h>
//## auto_generated
#include <oxf/omreactive.h>
//## auto_generated
#include <oxf/state.h>
//## auto_generated
#include <oxf/event.h>
//## auto_generated
#include <oxf/omlist.h>
//## link itsSterownik_1
#include "Sterownik.h"
//## link itsUstawienia
class Ustawienia;

//## package Default

//## class Klimatyzacja
class Klimatyzacja : public OMReactive {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedKlimatyzacja;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    Klimatyzacja(IOxfActive* theActiveContext = 0);
    
    //## auto_generated
    ~Klimatyzacja();
    
    ////    Additional operations    ////
    
    //## auto_generated
    Sterownik* getItsSterownik() const;
    
    //## auto_generated
    void setItsSterownik(Sterownik* p_Sterownik);
    
    //## auto_generated
    Sterownik* getItsSterownik_1() const;
    
    //## auto_generated
    OMIterator<Ustawienia*> getItsUstawienia() const;
    
    //## auto_generated
    Ustawienia* newItsUstawienia();
    
    //## auto_generated
    void deleteItsUstawienia(Ustawienia* p_Ustawienia);
    
    //## auto_generated
    virtual bool startBehavior();

protected :

    //## auto_generated
    void initRelations();
    
    //## auto_generated
    void cleanUpRelations();
    
    ////    Relations and components    ////
    
    Sterownik* itsSterownik;		//## link itsSterownik
    
    Sterownik itsSterownik_1;		//## link itsSterownik_1
    
    OMList<Ustawienia*> itsUstawienia;		//## link itsUstawienia
    
    ////    Framework operations    ////

public :

    //## auto_generated
    void __setItsSterownik(Sterownik* p_Sterownik);
    
    //## auto_generated
    void _setItsSterownik(Sterownik* p_Sterownik);
    
    //## auto_generated
    void _clearItsSterownik();
    
    //## auto_generated
    void _addItsUstawienia(Ustawienia* p_Ustawienia);
    
    //## auto_generated
    void _removeItsUstawienia(Ustawienia* p_Ustawienia);
    
    //## auto_generated
    void setActiveContext(IOxfActive* theActiveContext, bool activeInstance);
    
    //## auto_generated
    virtual void destroy();
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedKlimatyzacja : virtual public AOMInstance {
    DECLARE_META(Klimatyzacja, OMAnimatedKlimatyzacja)
    
    ////    Framework operations    ////
    
public :

    virtual void serializeRelations(AOMSRelations* aomsRelations) const;
};
//#]
#endif // _OMINSTRUMENT

#endif
/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/Klimatyzacja.h
*********************************************************************/
