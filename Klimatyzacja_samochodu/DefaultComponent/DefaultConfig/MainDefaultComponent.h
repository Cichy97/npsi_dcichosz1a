/*********************************************************************
	Rhapsody	: 8.3.1 
	Login		: student
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: DefaultConfig
//!	Generated Date	: Mon, 31, Aug 2020  
	File Path	: DefaultComponent/DefaultConfig/MainDefaultComponent.h
*********************************************************************/

#ifndef MainDefaultComponent_H
#define MainDefaultComponent_H

//## auto_generated
#include <oxf/oxf.h>
//## auto_generated
#include <aom/aom.h>
#endif
/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/MainDefaultComponent.h
*********************************************************************/
