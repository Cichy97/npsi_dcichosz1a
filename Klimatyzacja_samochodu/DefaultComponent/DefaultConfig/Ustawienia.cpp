/********************************************************************
	Rhapsody	: 8.3.1 
	Login		: student
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: Ustawienia
//!	Generated Date	: Mon, 31, Aug 2020  
	File Path	: DefaultComponent/DefaultConfig/Ustawienia.cpp
*********************************************************************/

//#[ ignore
#define NAMESPACE_PREFIX
//#]

//## auto_generated
#include "Ustawienia.h"
//## link itsKlimatyzacja
#include "Klimatyzacja.h"
//#[ ignore
#define Default_Ustawienia_Ustawienia_SERIALIZE OM_NO_OP
//#]

//## package Default

//## class Ustawienia
Ustawienia::Ustawienia() {
    NOTIFY_CONSTRUCTOR(Ustawienia, Ustawienia(), 0, Default_Ustawienia_Ustawienia_SERIALIZE);
    itsKlimatyzacja = NULL;
}

Ustawienia::~Ustawienia() {
    NOTIFY_DESTRUCTOR(~Ustawienia, true);
    cleanUpRelations();
}

Klimatyzacja* Ustawienia::getItsKlimatyzacja() const {
    return itsKlimatyzacja;
}

void Ustawienia::setItsKlimatyzacja(Klimatyzacja* p_Klimatyzacja) {
    if(p_Klimatyzacja != NULL)
        {
            p_Klimatyzacja->_addItsUstawienia(this);
        }
    _setItsKlimatyzacja(p_Klimatyzacja);
}

void Ustawienia::cleanUpRelations() {
    if(itsKlimatyzacja != NULL)
        {
            NOTIFY_RELATION_CLEARED("itsKlimatyzacja");
            Klimatyzacja* current = itsKlimatyzacja;
            if(current != NULL)
                {
                    current->_removeItsUstawienia(this);
                }
            itsKlimatyzacja = NULL;
        }
}

void Ustawienia::__setItsKlimatyzacja(Klimatyzacja* p_Klimatyzacja) {
    itsKlimatyzacja = p_Klimatyzacja;
    if(p_Klimatyzacja != NULL)
        {
            NOTIFY_RELATION_ITEM_ADDED("itsKlimatyzacja", p_Klimatyzacja, false, true);
        }
    else
        {
            NOTIFY_RELATION_CLEARED("itsKlimatyzacja");
        }
}

void Ustawienia::_setItsKlimatyzacja(Klimatyzacja* p_Klimatyzacja) {
    if(itsKlimatyzacja != NULL)
        {
            itsKlimatyzacja->_removeItsUstawienia(this);
        }
    __setItsKlimatyzacja(p_Klimatyzacja);
}

void Ustawienia::_clearItsKlimatyzacja() {
    NOTIFY_RELATION_CLEARED("itsKlimatyzacja");
    itsKlimatyzacja = NULL;
}

#ifdef _OMINSTRUMENT
//#[ ignore
void OMAnimatedUstawienia::serializeRelations(AOMSRelations* aomsRelations) const {
    aomsRelations->addRelation("itsKlimatyzacja", false, true);
    if(myReal->itsKlimatyzacja)
        {
            aomsRelations->ADD_ITEM(myReal->itsKlimatyzacja);
        }
}
//#]

IMPLEMENT_META_P(Ustawienia, Default, Default, false, OMAnimatedUstawienia)
#endif // _OMINSTRUMENT

/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/Ustawienia.cpp
*********************************************************************/
