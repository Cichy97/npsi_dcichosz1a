/*********************************************************************
	Rhapsody	: 8.3.1 
	Login		: student
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: Sterownik
//!	Generated Date	: Mon, 31, Aug 2020  
	File Path	: DefaultComponent/DefaultConfig/Sterownik.h
*********************************************************************/

#ifndef Sterownik_H
#define Sterownik_H

//## auto_generated
#include <oxf/oxf.h>
//## auto_generated
#include <aom/aom.h>
//## auto_generated
#include "Default.h"
//## auto_generated
#include <oxf/omthread.h>
//## auto_generated
#include <oxf/omreactive.h>
//## auto_generated
#include <oxf/state.h>
//## auto_generated
#include <oxf/event.h>
//## link itsKlimatyzacja
#include "Klimatyzacja.h"
//## link itsObieg_nagrzewnicy
#include "Obieg_nagrzewnicy.h"
//## link itsPanel_Klimatyzacji
#include "Panel_Klimatyzacji.h"
//## link itsWentylator
#include "Wentylator.h"
//## package Default

//## class Sterownik
class Sterownik : public OMReactive {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedSterownik;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    Sterownik(IOxfActive* theActiveContext = 0);
    
    //## auto_generated
    ~Sterownik();
    
    ////    Additional operations    ////
    
    //## auto_generated
    int getImpulsOK() const;
    
    //## auto_generated
    void setImpulsOK(int p_impulsOK);
    
    //## auto_generated
    int getStan() const;
    
    //## auto_generated
    void setStan(int p_stan);
    
    //## auto_generated
    Klimatyzacja* getItsKlimatyzacja() const;
    
    //## auto_generated
    Klimatyzacja* getItsKlimatyzacja_1() const;
    
    //## auto_generated
    void setItsKlimatyzacja_1(Klimatyzacja* p_Klimatyzacja);
    
    //## auto_generated
    Obieg_nagrzewnicy* getItsObieg_nagrzewnicy() const;
    
    //## auto_generated
    Panel_Klimatyzacji* getItsPanel_Klimatyzacji() const;
    
    //## auto_generated
    Wentylator* getItsWentylator() const;
    
    //## auto_generated
    virtual bool startBehavior();

protected :

    //## auto_generated
    void initRelations();
    
    //## auto_generated
    void cleanUpRelations();
    
    ////    Attributes    ////
    
    int impulsOK;		//## attribute impulsOK
    
    int stan;		//## attribute stan
    
    ////    Relations and components    ////
    
    Klimatyzacja itsKlimatyzacja;		//## link itsKlimatyzacja
    
    Klimatyzacja* itsKlimatyzacja_1;		//## link itsKlimatyzacja_1
    
    Obieg_nagrzewnicy itsObieg_nagrzewnicy;		//## link itsObieg_nagrzewnicy
    
    Panel_Klimatyzacji itsPanel_Klimatyzacji;		//## link itsPanel_Klimatyzacji
    
    Wentylator itsWentylator;		//## link itsWentylator
    
    ////    Framework operations    ////

public :

    //## auto_generated
    void __setItsKlimatyzacja_1(Klimatyzacja* p_Klimatyzacja);
    
    //## auto_generated
    void _setItsKlimatyzacja_1(Klimatyzacja* p_Klimatyzacja);
    
    //## auto_generated
    void _clearItsKlimatyzacja_1();
    
    //## auto_generated
    void setActiveContext(IOxfActive* theActiveContext, bool activeInstance);
    
    //## auto_generated
    virtual void destroy();
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedSterownik : virtual public AOMInstance {
    DECLARE_META(Sterownik, OMAnimatedSterownik)
    
    ////    Framework operations    ////
    
public :

    virtual void serializeAttributes(AOMSAttributes* aomsAttributes) const;
    
    virtual void serializeRelations(AOMSRelations* aomsRelations) const;
};
//#]
#endif // _OMINSTRUMENT

#endif
/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/Sterownik.h
*********************************************************************/
