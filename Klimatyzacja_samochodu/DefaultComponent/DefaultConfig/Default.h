/*********************************************************************
	Rhapsody	: 8.3.1 
	Login		: student
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: Default
//!	Generated Date	: Mon, 31, Aug 2020  
	File Path	: DefaultComponent/DefaultConfig/Default.h
*********************************************************************/

#ifndef Default_H
#define Default_H

//## auto_generated
#include <oxf/oxf.h>
//## auto_generated
#include <aom/aom.h>
//## auto_generated
#include <oxf/event.h>
//## auto_generated
class Klimatyzacja;

//## auto_generated
class Modul;

//## auto_generated
class Obieg_nagrzewnicy;

//## auto_generated
class Panel_Klimatyzacji;

//## auto_generated
class Sterownik;

//## auto_generated
class Ustawienia;

//## auto_generated
class Wentylator;

//#[ ignore
#define evimpuls_Default_id 18601

#define evDekoduj__Default_id 18602

#define eventmessage_0_Default_id 18603

#define evImpuls_Default_id 18604

#define evUruchom_Default_id 18605

#define evWylacz_Default_id 18606

#define evWlacz_Default_id 18607

#define evZewnetrzny_Default_id 18608

#define evWewnetrzny_Default_id 18609
//#]

//## package Default


//## type Stany
enum Stany {
    Wylaczony = 0,
    Wlaczony = 1
};

//## event evimpuls()
class evimpuls : public OMEvent {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedevimpuls;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    evimpuls();
    
    ////    Framework operations    ////
    
    //## statechart_method
    virtual bool isTypeOf(const short id) const;
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedevimpuls : virtual public AOMEvent {
    DECLARE_META_EVENT(evimpuls)
};
//#]
#endif // _OMINSTRUMENT

//## event evDekoduj_()
class evDekoduj_ : public OMEvent {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedevDekoduj_;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    evDekoduj_();
    
    ////    Framework operations    ////
    
    //## statechart_method
    virtual bool isTypeOf(const short id) const;
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedevDekoduj_ : virtual public AOMEvent {
    DECLARE_META_EVENT(evDekoduj_)
};
//#]
#endif // _OMINSTRUMENT

//## event eventmessage_0()
class eventmessage_0 : public OMEvent {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedeventmessage_0;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    eventmessage_0();
    
    ////    Framework operations    ////
    
    //## statechart_method
    virtual bool isTypeOf(const short id) const;
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedeventmessage_0 : virtual public AOMEvent {
    DECLARE_META_EVENT(eventmessage_0)
};
//#]
#endif // _OMINSTRUMENT

//## event evImpuls()
class evImpuls : public OMEvent {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedevImpuls;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    evImpuls();
    
    ////    Framework operations    ////
    
    //## statechart_method
    virtual bool isTypeOf(const short id) const;
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedevImpuls : virtual public AOMEvent {
    DECLARE_META_EVENT(evImpuls)
};
//#]
#endif // _OMINSTRUMENT

//## event evUruchom()
class evUruchom : public OMEvent {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedevUruchom;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    evUruchom();
    
    ////    Framework operations    ////
    
    //## statechart_method
    virtual bool isTypeOf(const short id) const;
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedevUruchom : virtual public AOMEvent {
    DECLARE_META_EVENT(evUruchom)
};
//#]
#endif // _OMINSTRUMENT

//## event evWylacz()
class evWylacz : public OMEvent {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedevWylacz;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    evWylacz();
    
    ////    Framework operations    ////
    
    //## statechart_method
    virtual bool isTypeOf(const short id) const;
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedevWylacz : virtual public AOMEvent {
    DECLARE_META_EVENT(evWylacz)
};
//#]
#endif // _OMINSTRUMENT

//## event evWlacz()
class evWlacz : public OMEvent {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedevWlacz;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    evWlacz();
    
    ////    Framework operations    ////
    
    //## statechart_method
    virtual bool isTypeOf(const short id) const;
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedevWlacz : virtual public AOMEvent {
    DECLARE_META_EVENT(evWlacz)
};
//#]
#endif // _OMINSTRUMENT

//## event evZewnetrzny()
class evZewnetrzny : public OMEvent {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedevZewnetrzny;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    evZewnetrzny();
    
    ////    Framework operations    ////
    
    //## statechart_method
    virtual bool isTypeOf(const short id) const;
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedevZewnetrzny : virtual public AOMEvent {
    DECLARE_META_EVENT(evZewnetrzny)
};
//#]
#endif // _OMINSTRUMENT

//## event evWewnetrzny()
class evWewnetrzny : public OMEvent {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedevWewnetrzny;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    evWewnetrzny();
    
    ////    Framework operations    ////
    
    //## statechart_method
    virtual bool isTypeOf(const short id) const;
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedevWewnetrzny : virtual public AOMEvent {
    DECLARE_META_EVENT(evWewnetrzny)
};
//#]
#endif // _OMINSTRUMENT

#endif
/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/Default.h
*********************************************************************/
