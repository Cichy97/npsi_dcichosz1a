/********************************************************************
	Rhapsody	: 8.3.1 
	Login		: student
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: Obieg_nagrzewnicy
//!	Generated Date	: Mon, 31, Aug 2020  
	File Path	: DefaultComponent/DefaultConfig/Obieg_nagrzewnicy.cpp
*********************************************************************/

//#[ ignore
#define NAMESPACE_PREFIX
//#]

//## auto_generated
#include "Obieg_nagrzewnicy.h"
//## link itsSterownik
#include "Sterownik.h"
//#[ ignore
#define Default_Obieg_nagrzewnicy_Obieg_nagrzewnicy_SERIALIZE OM_NO_OP
//#]

//## package Default

//## class Obieg_nagrzewnicy
Obieg_nagrzewnicy::Obieg_nagrzewnicy() {
    NOTIFY_CONSTRUCTOR(Obieg_nagrzewnicy, Obieg_nagrzewnicy(), 0, Default_Obieg_nagrzewnicy_Obieg_nagrzewnicy_SERIALIZE);
    itsSterownik = NULL;
}

Obieg_nagrzewnicy::~Obieg_nagrzewnicy() {
    NOTIFY_DESTRUCTOR(~Obieg_nagrzewnicy, false);
    cleanUpRelations();
}

Sterownik* Obieg_nagrzewnicy::getItsSterownik() const {
    return itsSterownik;
}

void Obieg_nagrzewnicy::setItsSterownik(Sterownik* p_Sterownik) {
    _setItsSterownik(p_Sterownik);
}

void Obieg_nagrzewnicy::cleanUpRelations() {
    if(itsSterownik != NULL)
        {
            NOTIFY_RELATION_CLEARED("itsSterownik");
            itsSterownik = NULL;
        }
}

void Obieg_nagrzewnicy::__setItsSterownik(Sterownik* p_Sterownik) {
    itsSterownik = p_Sterownik;
    if(p_Sterownik != NULL)
        {
            NOTIFY_RELATION_ITEM_ADDED("itsSterownik", p_Sterownik, false, true);
        }
    else
        {
            NOTIFY_RELATION_CLEARED("itsSterownik");
        }
}

void Obieg_nagrzewnicy::_setItsSterownik(Sterownik* p_Sterownik) {
    __setItsSterownik(p_Sterownik);
}

void Obieg_nagrzewnicy::_clearItsSterownik() {
    NOTIFY_RELATION_CLEARED("itsSterownik");
    itsSterownik = NULL;
}

#ifdef _OMINSTRUMENT
//#[ ignore
void OMAnimatedObieg_nagrzewnicy::serializeAttributes(AOMSAttributes* aomsAttributes) const {
    OMAnimatedModul::serializeAttributes(aomsAttributes);
}

void OMAnimatedObieg_nagrzewnicy::serializeRelations(AOMSRelations* aomsRelations) const {
    aomsRelations->addRelation("itsSterownik", false, true);
    if(myReal->itsSterownik)
        {
            aomsRelations->ADD_ITEM(myReal->itsSterownik);
        }
    OMAnimatedModul::serializeRelations(aomsRelations);
}
//#]

IMPLEMENT_META_S_P(Obieg_nagrzewnicy, Default, false, Modul, OMAnimatedModul, OMAnimatedObieg_nagrzewnicy)

OMINIT_SUPERCLASS(Modul, OMAnimatedModul)

OMREGISTER_CLASS
#endif // _OMINSTRUMENT

/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/Obieg_nagrzewnicy.cpp
*********************************************************************/
