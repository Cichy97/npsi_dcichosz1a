/********************************************************************
	Rhapsody	: 8.3.1 
	Login		: student
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: Klimatyzacja
//!	Generated Date	: Mon, 31, Aug 2020  
	File Path	: DefaultComponent/DefaultConfig/Klimatyzacja.cpp
*********************************************************************/

//#[ ignore
#define NAMESPACE_PREFIX
//#]

//## auto_generated
#include "Klimatyzacja.h"
//## link itsUstawienia
#include "Ustawienia.h"
//#[ ignore
#define Default_Klimatyzacja_Klimatyzacja_SERIALIZE OM_NO_OP
//#]

//## package Default

//## class Klimatyzacja
Klimatyzacja::Klimatyzacja(IOxfActive* theActiveContext) {
    NOTIFY_REACTIVE_CONSTRUCTOR(Klimatyzacja, Klimatyzacja(), 0, Default_Klimatyzacja_Klimatyzacja_SERIALIZE);
    setActiveContext(theActiveContext, false);
    {
        {
            itsSterownik_1.setShouldDelete(false);
        }
    }
    itsSterownik = NULL;
    initRelations();
}

Klimatyzacja::~Klimatyzacja() {
    NOTIFY_DESTRUCTOR(~Klimatyzacja, true);
    cleanUpRelations();
}

Sterownik* Klimatyzacja::getItsSterownik() const {
    return itsSterownik;
}

void Klimatyzacja::setItsSterownik(Sterownik* p_Sterownik) {
    _setItsSterownik(p_Sterownik);
}

Sterownik* Klimatyzacja::getItsSterownik_1() const {
    return (Sterownik*) &itsSterownik_1;
}

OMIterator<Ustawienia*> Klimatyzacja::getItsUstawienia() const {
    OMIterator<Ustawienia*> iter(itsUstawienia);
    return iter;
}

Ustawienia* Klimatyzacja::newItsUstawienia() {
    Ustawienia* newUstawienia = new Ustawienia;
    newUstawienia->_setItsKlimatyzacja(this);
    itsUstawienia.add(newUstawienia);
    NOTIFY_RELATION_ITEM_ADDED("itsUstawienia", newUstawienia, true, false);
    return newUstawienia;
}

void Klimatyzacja::deleteItsUstawienia(Ustawienia* p_Ustawienia) {
    p_Ustawienia->_setItsKlimatyzacja(NULL);
    itsUstawienia.remove(p_Ustawienia);
    NOTIFY_RELATION_ITEM_REMOVED("itsUstawienia", p_Ustawienia);
    delete p_Ustawienia;
}

bool Klimatyzacja::startBehavior() {
    bool done = true;
    done &= itsSterownik_1.startBehavior();
    done &= OMReactive::startBehavior();
    return done;
}

void Klimatyzacja::initRelations() {
    itsSterownik_1._setItsKlimatyzacja_1(this);
}

void Klimatyzacja::cleanUpRelations() {
    {
        OMIterator<Ustawienia*> iter(itsUstawienia);
        while (*iter){
            deleteItsUstawienia(*iter);
            iter.reset();
        }
    }
    if(itsSterownik != NULL)
        {
            NOTIFY_RELATION_CLEARED("itsSterownik");
            itsSterownik = NULL;
        }
}

void Klimatyzacja::__setItsSterownik(Sterownik* p_Sterownik) {
    itsSterownik = p_Sterownik;
    if(p_Sterownik != NULL)
        {
            NOTIFY_RELATION_ITEM_ADDED("itsSterownik", p_Sterownik, false, true);
        }
    else
        {
            NOTIFY_RELATION_CLEARED("itsSterownik");
        }
}

void Klimatyzacja::_setItsSterownik(Sterownik* p_Sterownik) {
    __setItsSterownik(p_Sterownik);
}

void Klimatyzacja::_clearItsSterownik() {
    NOTIFY_RELATION_CLEARED("itsSterownik");
    itsSterownik = NULL;
}

void Klimatyzacja::_addItsUstawienia(Ustawienia* p_Ustawienia) {
    if(p_Ustawienia != NULL)
        {
            NOTIFY_RELATION_ITEM_ADDED("itsUstawienia", p_Ustawienia, false, false);
        }
    else
        {
            NOTIFY_RELATION_CLEARED("itsUstawienia");
        }
    itsUstawienia.add(p_Ustawienia);
}

void Klimatyzacja::_removeItsUstawienia(Ustawienia* p_Ustawienia) {
    NOTIFY_RELATION_ITEM_REMOVED("itsUstawienia", p_Ustawienia);
    itsUstawienia.remove(p_Ustawienia);
}

void Klimatyzacja::setActiveContext(IOxfActive* theActiveContext, bool activeInstance) {
    OMReactive::setActiveContext(theActiveContext, activeInstance);
    {
        itsSterownik_1.setActiveContext(theActiveContext, false);
    }
}

void Klimatyzacja::destroy() {
    itsSterownik_1.destroy();
    OMReactive::destroy();
}

#ifdef _OMINSTRUMENT
//#[ ignore
void OMAnimatedKlimatyzacja::serializeRelations(AOMSRelations* aomsRelations) const {
    aomsRelations->addRelation("itsSterownik", false, true);
    if(myReal->itsSterownik)
        {
            aomsRelations->ADD_ITEM(myReal->itsSterownik);
        }
    aomsRelations->addRelation("itsSterownik_1", true, true);
    aomsRelations->ADD_ITEM(&myReal->itsSterownik_1);
    aomsRelations->addRelation("itsUstawienia", true, false);
    {
        OMIterator<Ustawienia*> iter(myReal->itsUstawienia);
        while (*iter){
            aomsRelations->ADD_ITEM(*iter);
            iter++;
        }
    }
}
//#]

IMPLEMENT_REACTIVE_META_SIMPLE_P(Klimatyzacja, Default, Default, false, OMAnimatedKlimatyzacja)
#endif // _OMINSTRUMENT

/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/Klimatyzacja.cpp
*********************************************************************/
