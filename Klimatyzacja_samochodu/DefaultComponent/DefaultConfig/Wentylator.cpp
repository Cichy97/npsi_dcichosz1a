/********************************************************************
	Rhapsody	: 8.3.1 
	Login		: student
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: Wentylator
//!	Generated Date	: Mon, 31, Aug 2020  
	File Path	: DefaultComponent/DefaultConfig/Wentylator.cpp
*********************************************************************/

//#[ ignore
#define NAMESPACE_PREFIX
//#]

//## auto_generated
#include "Wentylator.h"
//## link itsSterownik
#include "Sterownik.h"
//#[ ignore
#define Default_Wentylator_Wentylator_SERIALIZE OM_NO_OP
//#]

//## package Default

//## class Wentylator
Wentylator::Wentylator(IOxfActive* theActiveContext) {
    NOTIFY_REACTIVE_CONSTRUCTOR(Wentylator, Wentylator(), 0, Default_Wentylator_Wentylator_SERIALIZE);
    setActiveContext(theActiveContext, false);
    itsSterownik = NULL;
}

Wentylator::~Wentylator() {
    NOTIFY_DESTRUCTOR(~Wentylator, false);
    cleanUpRelations();
}

Sterownik* Wentylator::getItsSterownik() const {
    return itsSterownik;
}

void Wentylator::setItsSterownik(Sterownik* p_Sterownik) {
    _setItsSterownik(p_Sterownik);
}

bool Wentylator::startBehavior() {
    bool done = false;
    done = OMReactive::startBehavior();
    return done;
}

void Wentylator::cleanUpRelations() {
    if(itsSterownik != NULL)
        {
            NOTIFY_RELATION_CLEARED("itsSterownik");
            itsSterownik = NULL;
        }
}

void Wentylator::__setItsSterownik(Sterownik* p_Sterownik) {
    itsSterownik = p_Sterownik;
    if(p_Sterownik != NULL)
        {
            NOTIFY_RELATION_ITEM_ADDED("itsSterownik", p_Sterownik, false, true);
        }
    else
        {
            NOTIFY_RELATION_CLEARED("itsSterownik");
        }
}

void Wentylator::_setItsSterownik(Sterownik* p_Sterownik) {
    __setItsSterownik(p_Sterownik);
}

void Wentylator::_clearItsSterownik() {
    NOTIFY_RELATION_CLEARED("itsSterownik");
    itsSterownik = NULL;
}

#ifdef _OMINSTRUMENT
//#[ ignore
void OMAnimatedWentylator::serializeAttributes(AOMSAttributes* aomsAttributes) const {
    OMAnimatedModul::serializeAttributes(aomsAttributes);
}

void OMAnimatedWentylator::serializeRelations(AOMSRelations* aomsRelations) const {
    aomsRelations->addRelation("itsSterownik", false, true);
    if(myReal->itsSterownik)
        {
            aomsRelations->ADD_ITEM(myReal->itsSterownik);
        }
    OMAnimatedModul::serializeRelations(aomsRelations);
}
//#]

IMPLEMENT_REACTIVE_META_S_SIMPLE_P(Wentylator, Default, false, Modul, OMAnimatedModul, OMAnimatedWentylator)

OMINIT_SUPERCLASS(Modul, OMAnimatedModul)

OMREGISTER_REACTIVE_CLASS
#endif // _OMINSTRUMENT

/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/Wentylator.cpp
*********************************************************************/
