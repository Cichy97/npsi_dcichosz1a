
############# Target type (Debug/Release) ##################
############################################################
CPPCompileDebug=-g
CPPCompileRelease=-O
LinkDebug=-g
LinkRelease=-O

CleanupFlagForSimulink=
SIMULINK_CONFIG=False
ifeq ($(SIMULINK_CONFIG),True)
CleanupFlagForSimulink=-DOM_WITH_CLEANUP
endif

ConfigurationCPPCompileSwitches=   $(INCLUDE_QUALIFIER). $(INCLUDE_QUALIFIER)$(OMROOT) $(INCLUDE_QUALIFIER)$(OMROOT)/LangCpp $(INCLUDE_QUALIFIER)$(OMROOT)/LangCpp/oxf $(DEFINE_QUALIFIER)CYGWIN $(INST_FLAGS) $(INCLUDE_PATH) $(INST_INCLUDES) -Wno-write-strings $(CPPCompileDebug) -c  $(CleanupFlagForSimulink)
ConfigurationCCCompileSwitches=$(INCLUDE_PATH) -c 

#########################################
###### Predefined macros ################
RM=/bin/rm -rf
INCLUDE_QUALIFIER=-I
DEFINE_QUALIFIER=-D
CC=g++
LIB_CMD=ar
LINK_CMD=g++
LIB_FLAGS=rvu
LINK_FLAGS= $(LinkDebug)   

#########################################
####### Context macros ##################

FLAGSFILE=
RULESFILE=
OMROOT="C:/ProgramData/IBM/Rational/Rhapsody/8.3.1/Share"
RHPROOT="C:/Program Files/IBM/Rational/Rhapsody/8.3.1"

CPP_EXT=.cpp
H_EXT=.h
OBJ_EXT=.o
EXE_EXT=.exe
LIB_EXT=.a

INSTRUMENTATION=Animation

TIME_MODEL=RealTime

TARGET_TYPE=Executable

TARGET_NAME=DefaultComponent

all : $(TARGET_NAME)$(EXE_EXT) DefaultComponent.mak

TARGET_MAIN=MainDefaultComponent

LIBS=

INCLUDE_PATH= \
  $(INCLUDE_QUALIFIER)$(OMROOT)/LangCpp/osconfig/Cygwin

ADDITIONAL_OBJS=

OBJS= \
  Sterownik.o \
  Wentylator.o \
  Obieg_nagrzewnicy.o \
  Panel_Klimatyzacji.o \
  Klimatyzacja.o \
  Modul.o \
  Ustawienia.o \
  Default.o




#########################################
####### Predefined macros ###############
$(OBJS) : $(INST_LIBS) $(OXF_LIBS)

ifeq ($(INSTRUMENTATION),Animation)

INST_FLAGS=$(DEFINE_QUALIFIER)OMANIMATOR $(DEFINE_QUALIFIER)__USE_W32_SOCKETS 
INST_INCLUDES=$(INCLUDE_QUALIFIER)$(OMROOT)/LangCpp/aom $(INCLUDE_QUALIFIER)$(OMROOT)/LangCpp/tom
INST_LIBS=$(OMROOT)/LangCpp/lib/cygwinaomanim$(LIB_EXT) $(OMROOT)/LangCpp/lib/cygwinoxsiminst$(LIB_EXT)
OXF_LIBS=$(OMROOT)/LangCpp/lib/cygwinoxfinst$(LIB_EXT) $(OMROOT)/LangCpp/lib/cygwinomcomappl$(LIB_EXT)
SOCK_LIB=-lws2_32

else
ifeq ($(INSTRUMENTATION),Tracing)

INST_FLAGS=$(DEFINE_QUALIFIER)OMTRACER 
INST_INCLUDES=$(INCLUDE_QUALIFIER)$(OMROOT)/LangCpp/aom $(INCLUDE_QUALIFIER)$(OMROOT)/LangCpp/tom
INST_LIBS=$(OMROOT)/LangCpp/lib/cygwintomtrace$(LIB_EXT) $(OMROOT)/LangCpp/lib/cygwinaomtrace$(LIB_EXT) $(OMROOT)/LangCpp/lib/cygwinoxsiminst$(LIB_EXT)
OXF_LIBS=$(OMROOT)/LangCpp/lib/cygwinoxfinst$(LIB_EXT) $(OMROOT)/LangCpp/lib/cygwinomcomappl$(LIB_EXT)
SOCK_LIB=-lws2_32

else
ifeq ($(INSTRUMENTATION),None)

INST_FLAGS= 
INST_INCLUDES=
INST_LIBS=$(OMROOT)/LangCpp/lib/cygwinoxsim$(LIB_EXT)
OXF_LIBS=$(OMROOT)/LangCpp/lib/cygwinoxf$(LIB_EXT)
SOCK_LIB=-lws2_32

else
	@echo An invalid Instrumentation $(INSTRUMENTATION) is specified.
	exit
endif
endif
endif

.SUFFIXES: $(CPP_EXT)

#####################################################################
##################### Context dependencies and commands #############






Sterownik.o : Sterownik.cpp Sterownik.h    Default.h Klimatyzacja.h Panel_Klimatyzacji.h Wentylator.h Obieg_nagrzewnicy.h Modul.h 
	@echo Compiling Sterownik.cpp
	@$(CC) $(ConfigurationCPPCompileSwitches)  -o Sterownik.o Sterownik.cpp




Wentylator.o : Wentylator.cpp Wentylator.h    Default.h Sterownik.h Modul.h 
	@echo Compiling Wentylator.cpp
	@$(CC) $(ConfigurationCPPCompileSwitches)  -o Wentylator.o Wentylator.cpp




Obieg_nagrzewnicy.o : Obieg_nagrzewnicy.cpp Obieg_nagrzewnicy.h    Default.h Sterownik.h Modul.h 
	@echo Compiling Obieg_nagrzewnicy.cpp
	@$(CC) $(ConfigurationCPPCompileSwitches)  -o Obieg_nagrzewnicy.o Obieg_nagrzewnicy.cpp




Panel_Klimatyzacji.o : Panel_Klimatyzacji.cpp Panel_Klimatyzacji.h    Default.h Sterownik.h Modul.h 
	@echo Compiling Panel_Klimatyzacji.cpp
	@$(CC) $(ConfigurationCPPCompileSwitches)  -o Panel_Klimatyzacji.o Panel_Klimatyzacji.cpp




Klimatyzacja.o : Klimatyzacja.cpp Klimatyzacja.h    Default.h Sterownik.h Ustawienia.h Modul.h 
	@echo Compiling Klimatyzacja.cpp
	@$(CC) $(ConfigurationCPPCompileSwitches)  -o Klimatyzacja.o Klimatyzacja.cpp




Modul.o : Modul.cpp Modul.h    Default.h 
	@echo Compiling Modul.cpp
	@$(CC) $(ConfigurationCPPCompileSwitches)  -o Modul.o Modul.cpp




Ustawienia.o : Ustawienia.cpp Ustawienia.h    Default.h Klimatyzacja.h 
	@echo Compiling Ustawienia.cpp
	@$(CC) $(ConfigurationCPPCompileSwitches)  -o Ustawienia.o Ustawienia.cpp




Default.o : Default.cpp Default.h    Sterownik.h Wentylator.h Obieg_nagrzewnicy.h Panel_Klimatyzacji.h Klimatyzacja.h Modul.h Ustawienia.h 
	@echo Compiling Default.cpp
	@$(CC) $(ConfigurationCPPCompileSwitches)  -o Default.o Default.cpp







$(TARGET_MAIN)$(OBJ_EXT) : $(TARGET_MAIN)$(CPP_EXT) $(OBJS)
	@echo Compiling $(TARGET_MAIN)$(CPP_EXT)
	@$(CC) $(ConfigurationCPPCompileSwitches) -o  $(TARGET_MAIN)$(OBJ_EXT) $(TARGET_MAIN)$(CPP_EXT)

####################################################################
############## Predefined Instructions #############################
$(TARGET_NAME)$(EXE_EXT): $(OBJS) $(ADDITIONAL_OBJS) $(TARGET_MAIN)$(OBJ_EXT) DefaultComponent.mak
	@echo Linking $(TARGET_NAME)$(EXE_EXT)
	@$(LINK_CMD)  $(TARGET_MAIN)$(OBJ_EXT) $(OBJS) $(ADDITIONAL_OBJS) \
	$(LIBS) \
	$(OXF_LIBS) \
	$(INST_LIBS) \
	$(OXF_LIBS) \
	$(INST_LIBS) \
	$(SOCK_LIB) \
	$(LINK_FLAGS) -o $(TARGET_NAME)$(EXE_EXT)

$(TARGET_NAME)$(LIB_EXT) : $(OBJS) $(ADDITIONAL_OBJS) DefaultComponent.mak
	@echo Building library $@
	@$(LIB_CMD) $(LIB_FLAGS) $(TARGET_NAME)$(LIB_EXT) $(OBJS) $(ADDITIONAL_OBJS)



clean:
	@echo Cleanup
	$(RM) Sterownik.o
	$(RM) Wentylator.o
	$(RM) Obieg_nagrzewnicy.o
	$(RM) Panel_Klimatyzacji.o
	$(RM) Klimatyzacja.o
	$(RM) Modul.o
	$(RM) Ustawienia.o
	$(RM) Default.o
	$(RM) $(TARGET_MAIN)$(OBJ_EXT) $(ADDITIONAL_OBJS)
	$(RM) $(TARGET_NAME)$(LIB_EXT)
	$(RM) $(TARGET_NAME)$(EXE_EXT)

