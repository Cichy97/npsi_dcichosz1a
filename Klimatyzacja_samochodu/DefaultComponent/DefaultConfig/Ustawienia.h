/*********************************************************************
	Rhapsody	: 8.3.1 
	Login		: student
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: Ustawienia
//!	Generated Date	: Mon, 31, Aug 2020  
	File Path	: DefaultComponent/DefaultConfig/Ustawienia.h
*********************************************************************/

#ifndef Ustawienia_H
#define Ustawienia_H

//## auto_generated
#include <oxf/oxf.h>
//## auto_generated
#include <aom/aom.h>
//## auto_generated
#include "Default.h"
//## link itsKlimatyzacja
class Klimatyzacja;

//## package Default

//## class Ustawienia
class Ustawienia {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedUstawienia;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    Ustawienia();
    
    //## auto_generated
    ~Ustawienia();
    
    ////    Additional operations    ////
    
    //## auto_generated
    Klimatyzacja* getItsKlimatyzacja() const;
    
    //## auto_generated
    void setItsKlimatyzacja(Klimatyzacja* p_Klimatyzacja);

protected :

    //## auto_generated
    void cleanUpRelations();
    
    ////    Relations and components    ////
    
    Klimatyzacja* itsKlimatyzacja;		//## link itsKlimatyzacja
    
    ////    Framework operations    ////

public :

    //## auto_generated
    void __setItsKlimatyzacja(Klimatyzacja* p_Klimatyzacja);
    
    //## auto_generated
    void _setItsKlimatyzacja(Klimatyzacja* p_Klimatyzacja);
    
    //## auto_generated
    void _clearItsKlimatyzacja();
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedUstawienia : virtual public AOMInstance {
    DECLARE_META(Ustawienia, OMAnimatedUstawienia)
    
    ////    Framework operations    ////
    
public :

    virtual void serializeRelations(AOMSRelations* aomsRelations) const;
};
//#]
#endif // _OMINSTRUMENT

#endif
/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/Ustawienia.h
*********************************************************************/
