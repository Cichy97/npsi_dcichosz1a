/********************************************************************
	Rhapsody	: 8.3.1 
	Login		: student
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: Default
//!	Generated Date	: Mon, 31, Aug 2020  
	File Path	: DefaultComponent/DefaultConfig/Default.cpp
*********************************************************************/

//#[ ignore
#define NAMESPACE_PREFIX
//#]

//## auto_generated
#include "Default.h"
//## auto_generated
#include "Klimatyzacja.h"
//## auto_generated
#include "Modul.h"
//## auto_generated
#include "Obieg_nagrzewnicy.h"
//## auto_generated
#include "Panel_Klimatyzacji.h"
//## auto_generated
#include "Sterownik.h"
//## auto_generated
#include "Ustawienia.h"
//## auto_generated
#include "Wentylator.h"
//#[ ignore
#define evimpuls_SERIALIZE OM_NO_OP

#define evimpuls_UNSERIALIZE OM_NO_OP

#define evimpuls_CONSTRUCTOR evimpuls()

#define evDekoduj__SERIALIZE OM_NO_OP

#define evDekoduj__UNSERIALIZE OM_NO_OP

#define evDekoduj__CONSTRUCTOR evDekoduj_()

#define eventmessage_0_SERIALIZE OM_NO_OP

#define eventmessage_0_UNSERIALIZE OM_NO_OP

#define eventmessage_0_CONSTRUCTOR eventmessage_0()

#define evImpuls_SERIALIZE OM_NO_OP

#define evImpuls_UNSERIALIZE OM_NO_OP

#define evImpuls_CONSTRUCTOR evImpuls()

#define evUruchom_SERIALIZE OM_NO_OP

#define evUruchom_UNSERIALIZE OM_NO_OP

#define evUruchom_CONSTRUCTOR evUruchom()

#define evWylacz_SERIALIZE OM_NO_OP

#define evWylacz_UNSERIALIZE OM_NO_OP

#define evWylacz_CONSTRUCTOR evWylacz()

#define evWlacz_SERIALIZE OM_NO_OP

#define evWlacz_UNSERIALIZE OM_NO_OP

#define evWlacz_CONSTRUCTOR evWlacz()

#define evZewnetrzny_SERIALIZE OM_NO_OP

#define evZewnetrzny_UNSERIALIZE OM_NO_OP

#define evZewnetrzny_CONSTRUCTOR evZewnetrzny()

#define evWewnetrzny_SERIALIZE OM_NO_OP

#define evWewnetrzny_UNSERIALIZE OM_NO_OP

#define evWewnetrzny_CONSTRUCTOR evWewnetrzny()
//#]

//## package Default


#ifdef _OMINSTRUMENT
static void serializeGlobalVars(AOMSAttributes* /* aomsAttributes */);

IMPLEMENT_META_PACKAGE(Default, Default)

static void serializeGlobalVars(AOMSAttributes* /* aomsAttributes */) {
}
#endif // _OMINSTRUMENT

//## event evimpuls()
evimpuls::evimpuls() {
    NOTIFY_EVENT_CONSTRUCTOR(evimpuls)
    setId(evimpuls_Default_id);
}

bool evimpuls::isTypeOf(const short id) const {
    return (evimpuls_Default_id==id);
}

IMPLEMENT_META_EVENT_P(evimpuls, Default, Default, evimpuls())

//## event evDekoduj_()
evDekoduj_::evDekoduj_() {
    NOTIFY_EVENT_CONSTRUCTOR(evDekoduj_)
    setId(evDekoduj__Default_id);
}

bool evDekoduj_::isTypeOf(const short id) const {
    return (evDekoduj__Default_id==id);
}

IMPLEMENT_META_EVENT_P(evDekoduj_, Default, Default, evDekoduj_())

//## event eventmessage_0()
eventmessage_0::eventmessage_0() {
    NOTIFY_EVENT_CONSTRUCTOR(eventmessage_0)
    setId(eventmessage_0_Default_id);
}

bool eventmessage_0::isTypeOf(const short id) const {
    return (eventmessage_0_Default_id==id);
}

IMPLEMENT_META_EVENT_P(eventmessage_0, Default, Default, eventmessage_0())

//## event evImpuls()
evImpuls::evImpuls() {
    NOTIFY_EVENT_CONSTRUCTOR(evImpuls)
    setId(evImpuls_Default_id);
}

bool evImpuls::isTypeOf(const short id) const {
    return (evImpuls_Default_id==id);
}

IMPLEMENT_META_EVENT_P(evImpuls, Default, Default, evImpuls())

//## event evUruchom()
evUruchom::evUruchom() {
    NOTIFY_EVENT_CONSTRUCTOR(evUruchom)
    setId(evUruchom_Default_id);
}

bool evUruchom::isTypeOf(const short id) const {
    return (evUruchom_Default_id==id);
}

IMPLEMENT_META_EVENT_P(evUruchom, Default, Default, evUruchom())

//## event evWylacz()
evWylacz::evWylacz() {
    NOTIFY_EVENT_CONSTRUCTOR(evWylacz)
    setId(evWylacz_Default_id);
}

bool evWylacz::isTypeOf(const short id) const {
    return (evWylacz_Default_id==id);
}

IMPLEMENT_META_EVENT_P(evWylacz, Default, Default, evWylacz())

//## event evWlacz()
evWlacz::evWlacz() {
    NOTIFY_EVENT_CONSTRUCTOR(evWlacz)
    setId(evWlacz_Default_id);
}

bool evWlacz::isTypeOf(const short id) const {
    return (evWlacz_Default_id==id);
}

IMPLEMENT_META_EVENT_P(evWlacz, Default, Default, evWlacz())

//## event evZewnetrzny()
evZewnetrzny::evZewnetrzny() {
    NOTIFY_EVENT_CONSTRUCTOR(evZewnetrzny)
    setId(evZewnetrzny_Default_id);
}

bool evZewnetrzny::isTypeOf(const short id) const {
    return (evZewnetrzny_Default_id==id);
}

IMPLEMENT_META_EVENT_P(evZewnetrzny, Default, Default, evZewnetrzny())

//## event evWewnetrzny()
evWewnetrzny::evWewnetrzny() {
    NOTIFY_EVENT_CONSTRUCTOR(evWewnetrzny)
    setId(evWewnetrzny_Default_id);
}

bool evWewnetrzny::isTypeOf(const short id) const {
    return (evWewnetrzny_Default_id==id);
}

IMPLEMENT_META_EVENT_P(evWewnetrzny, Default, Default, evWewnetrzny())

/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/Default.cpp
*********************************************************************/
