/********************************************************************
	Rhapsody	: 8.3.1 
	Login		: student
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: Panel_Klimatyzacji
//!	Generated Date	: Mon, 31, Aug 2020  
	File Path	: DefaultComponent/DefaultConfig/Panel_Klimatyzacji.cpp
*********************************************************************/

//#[ ignore
#define NAMESPACE_PREFIX
//#]

//## auto_generated
#include "Panel_Klimatyzacji.h"
//## link itsSterownik
#include "Sterownik.h"
//## event evWylacz()
#include "Default.h"
//#[ ignore
#define Default_Panel_Klimatyzacji_Panel_Klimatyzacji_SERIALIZE OM_NO_OP

#define Default_Panel_Klimatyzacji_Przycisk_Zwieksz_Temp_Przycisk_Zwieksz_Temp_SERIALIZE OM_NO_OP

#define Default_Panel_Klimatyzacji_Przycisk_Zwieksz_Nawiew_Przycisk_Zwieksz_Nawiew_SERIALIZE OM_NO_OP

#define Default_Panel_Klimatyzacji_Przycisk_Zmniejsz_Temp_Przycisk_Zmniejsz_Temp_SERIALIZE OM_NO_OP

#define Default_Panel_Klimatyzacji_Przycisk_Zmniejsz_Nawiew_Przycisk_Zmniejsz_Nawiew_SERIALIZE OM_NO_OP

#define Default_Panel_Klimatyzacji_Przycisk_Wylacz_Przycisk_Wylacz_SERIALIZE OM_NO_OP

#define Default_Panel_Klimatyzacji_Przycisk_Wlacz_Przycisk_Wlacz_SERIALIZE OM_NO_OP

#define Default_Panel_Klimatyzacji_Przycisk_Obieg_Zewnetrzny_Przycisk_Obieg_Zewnetrzny_SERIALIZE OM_NO_OP

#define Default_Panel_Klimatyzacji_Przycisk_Obieg_Wewnetrzny_Przycisk_Obieg_Wewnetrzny_SERIALIZE OM_NO_OP

#define Default_Panel_Klimatyzacji_Przycisk_Nadmuch_Srodkowy_Przycisk_Nadmuch_Srodkowy_SERIALIZE OM_NO_OP

#define Default_Panel_Klimatyzacji_Przycisk_Nadmuch_Na_Szybe_Przycisk_Nadmuch_Na_Szybe_SERIALIZE OM_NO_OP

#define Default_Panel_Klimatyzacji_Przycisk_Nadmuch_Na_Nogi_Przycisk_Nadmuch_Na_Nogi_SERIALIZE OM_NO_OP
//#]

//## package Default

//## class Panel_Klimatyzacji
//## class Panel_Klimatyzacji::Przycisk_Zwieksz_Temp
Panel_Klimatyzacji::Przycisk_Zwieksz_Temp::Przycisk_Zwieksz_Temp() {
    NOTIFY_CONSTRUCTOR(Przycisk_Zwieksz_Temp, Przycisk_Zwieksz_Temp(), 0, Default_Panel_Klimatyzacji_Przycisk_Zwieksz_Temp_Przycisk_Zwieksz_Temp_SERIALIZE);
}

Panel_Klimatyzacji::Przycisk_Zwieksz_Temp::~Przycisk_Zwieksz_Temp() {
    NOTIFY_DESTRUCTOR(~Przycisk_Zwieksz_Temp, true);
}

//## class Panel_Klimatyzacji::Przycisk_Zwieksz_Nawiew
Panel_Klimatyzacji::Przycisk_Zwieksz_Nawiew::Przycisk_Zwieksz_Nawiew() {
    NOTIFY_CONSTRUCTOR(Przycisk_Zwieksz_Nawiew, Przycisk_Zwieksz_Nawiew(), 0, Default_Panel_Klimatyzacji_Przycisk_Zwieksz_Nawiew_Przycisk_Zwieksz_Nawiew_SERIALIZE);
}

Panel_Klimatyzacji::Przycisk_Zwieksz_Nawiew::~Przycisk_Zwieksz_Nawiew() {
    NOTIFY_DESTRUCTOR(~Przycisk_Zwieksz_Nawiew, true);
}

//## class Panel_Klimatyzacji::Przycisk_Zmniejsz_Temp
Panel_Klimatyzacji::Przycisk_Zmniejsz_Temp::Przycisk_Zmniejsz_Temp() {
    NOTIFY_CONSTRUCTOR(Przycisk_Zmniejsz_Temp, Przycisk_Zmniejsz_Temp(), 0, Default_Panel_Klimatyzacji_Przycisk_Zmniejsz_Temp_Przycisk_Zmniejsz_Temp_SERIALIZE);
}

Panel_Klimatyzacji::Przycisk_Zmniejsz_Temp::~Przycisk_Zmniejsz_Temp() {
    NOTIFY_DESTRUCTOR(~Przycisk_Zmniejsz_Temp, true);
}

//## class Panel_Klimatyzacji::Przycisk_Zmniejsz_Nawiew
Panel_Klimatyzacji::Przycisk_Zmniejsz_Nawiew::Przycisk_Zmniejsz_Nawiew() {
    NOTIFY_CONSTRUCTOR(Przycisk_Zmniejsz_Nawiew, Przycisk_Zmniejsz_Nawiew(), 0, Default_Panel_Klimatyzacji_Przycisk_Zmniejsz_Nawiew_Przycisk_Zmniejsz_Nawiew_SERIALIZE);
}

Panel_Klimatyzacji::Przycisk_Zmniejsz_Nawiew::~Przycisk_Zmniejsz_Nawiew() {
    NOTIFY_DESTRUCTOR(~Przycisk_Zmniejsz_Nawiew, true);
}

//## class Panel_Klimatyzacji::Przycisk_Wylacz
Panel_Klimatyzacji::Przycisk_Wylacz::Przycisk_Wylacz(IOxfActive* theActiveContext) {
    NOTIFY_REACTIVE_CONSTRUCTOR(Przycisk_Wylacz, Przycisk_Wylacz(), 0, Default_Panel_Klimatyzacji_Przycisk_Wylacz_Przycisk_Wylacz_SERIALIZE);
    setActiveContext(theActiveContext, false);
}

Panel_Klimatyzacji::Przycisk_Wylacz::~Przycisk_Wylacz() {
    NOTIFY_DESTRUCTOR(~Przycisk_Wylacz, true);
}

bool Panel_Klimatyzacji::Przycisk_Wylacz::startBehavior() {
    bool done = false;
    done = OMReactive::startBehavior();
    return done;
}

//## class Panel_Klimatyzacji::Przycisk_Wlacz
Panel_Klimatyzacji::Przycisk_Wlacz::Przycisk_Wlacz(IOxfActive* theActiveContext) {
    NOTIFY_REACTIVE_CONSTRUCTOR(Przycisk_Wlacz, Przycisk_Wlacz(), 0, Default_Panel_Klimatyzacji_Przycisk_Wlacz_Przycisk_Wlacz_SERIALIZE);
    setActiveContext(theActiveContext, false);
}

Panel_Klimatyzacji::Przycisk_Wlacz::~Przycisk_Wlacz() {
    NOTIFY_DESTRUCTOR(~Przycisk_Wlacz, true);
}

bool Panel_Klimatyzacji::Przycisk_Wlacz::startBehavior() {
    bool done = false;
    done = OMReactive::startBehavior();
    return done;
}

//## class Panel_Klimatyzacji::Przycisk_Obieg_Zewnetrzny
Panel_Klimatyzacji::Przycisk_Obieg_Zewnetrzny::Przycisk_Obieg_Zewnetrzny() {
    NOTIFY_CONSTRUCTOR(Przycisk_Obieg_Zewnetrzny, Przycisk_Obieg_Zewnetrzny(), 0, Default_Panel_Klimatyzacji_Przycisk_Obieg_Zewnetrzny_Przycisk_Obieg_Zewnetrzny_SERIALIZE);
}

Panel_Klimatyzacji::Przycisk_Obieg_Zewnetrzny::~Przycisk_Obieg_Zewnetrzny() {
    NOTIFY_DESTRUCTOR(~Przycisk_Obieg_Zewnetrzny, true);
}

//## class Panel_Klimatyzacji::Przycisk_Obieg_Wewnetrzny
Panel_Klimatyzacji::Przycisk_Obieg_Wewnetrzny::Przycisk_Obieg_Wewnetrzny() {
    NOTIFY_CONSTRUCTOR(Przycisk_Obieg_Wewnetrzny, Przycisk_Obieg_Wewnetrzny(), 0, Default_Panel_Klimatyzacji_Przycisk_Obieg_Wewnetrzny_Przycisk_Obieg_Wewnetrzny_SERIALIZE);
}

Panel_Klimatyzacji::Przycisk_Obieg_Wewnetrzny::~Przycisk_Obieg_Wewnetrzny() {
    NOTIFY_DESTRUCTOR(~Przycisk_Obieg_Wewnetrzny, true);
}

//## class Panel_Klimatyzacji::Przycisk_Nadmuch_Srodkowy
Panel_Klimatyzacji::Przycisk_Nadmuch_Srodkowy::Przycisk_Nadmuch_Srodkowy() {
    NOTIFY_CONSTRUCTOR(Przycisk_Nadmuch_Srodkowy, Przycisk_Nadmuch_Srodkowy(), 0, Default_Panel_Klimatyzacji_Przycisk_Nadmuch_Srodkowy_Przycisk_Nadmuch_Srodkowy_SERIALIZE);
}

Panel_Klimatyzacji::Przycisk_Nadmuch_Srodkowy::~Przycisk_Nadmuch_Srodkowy() {
    NOTIFY_DESTRUCTOR(~Przycisk_Nadmuch_Srodkowy, true);
}

//## class Panel_Klimatyzacji::Przycisk_Nadmuch_Na_Szybe
Panel_Klimatyzacji::Przycisk_Nadmuch_Na_Szybe::Przycisk_Nadmuch_Na_Szybe() {
    NOTIFY_CONSTRUCTOR(Przycisk_Nadmuch_Na_Szybe, Przycisk_Nadmuch_Na_Szybe(), 0, Default_Panel_Klimatyzacji_Przycisk_Nadmuch_Na_Szybe_Przycisk_Nadmuch_Na_Szybe_SERIALIZE);
}

Panel_Klimatyzacji::Przycisk_Nadmuch_Na_Szybe::~Przycisk_Nadmuch_Na_Szybe() {
    NOTIFY_DESTRUCTOR(~Przycisk_Nadmuch_Na_Szybe, true);
}

//## class Panel_Klimatyzacji::Przycisk_Nadmuch_Na_Nogi
Panel_Klimatyzacji::Przycisk_Nadmuch_Na_Nogi::Przycisk_Nadmuch_Na_Nogi() {
    NOTIFY_CONSTRUCTOR(Przycisk_Nadmuch_Na_Nogi, Przycisk_Nadmuch_Na_Nogi(), 0, Default_Panel_Klimatyzacji_Przycisk_Nadmuch_Na_Nogi_Przycisk_Nadmuch_Na_Nogi_SERIALIZE);
}

Panel_Klimatyzacji::Przycisk_Nadmuch_Na_Nogi::~Przycisk_Nadmuch_Na_Nogi() {
    NOTIFY_DESTRUCTOR(~Przycisk_Nadmuch_Na_Nogi, true);
}

Panel_Klimatyzacji::Panel_Klimatyzacji() {
    NOTIFY_CONSTRUCTOR(Panel_Klimatyzacji, Panel_Klimatyzacji(), 0, Default_Panel_Klimatyzacji_Panel_Klimatyzacji_SERIALIZE);
    itsSterownik = NULL;
}

Panel_Klimatyzacji::~Panel_Klimatyzacji() {
    NOTIFY_DESTRUCTOR(~Panel_Klimatyzacji, false);
    cleanUpRelations();
}

Sterownik* Panel_Klimatyzacji::getItsSterownik() const {
    return itsSterownik;
}

void Panel_Klimatyzacji::setItsSterownik(Sterownik* p_Sterownik) {
    _setItsSterownik(p_Sterownik);
}

void Panel_Klimatyzacji::cleanUpRelations() {
    if(itsSterownik != NULL)
        {
            NOTIFY_RELATION_CLEARED("itsSterownik");
            itsSterownik = NULL;
        }
}

void Panel_Klimatyzacji::__setItsSterownik(Sterownik* p_Sterownik) {
    itsSterownik = p_Sterownik;
    if(p_Sterownik != NULL)
        {
            NOTIFY_RELATION_ITEM_ADDED("itsSterownik", p_Sterownik, false, true);
        }
    else
        {
            NOTIFY_RELATION_CLEARED("itsSterownik");
        }
}

void Panel_Klimatyzacji::_setItsSterownik(Sterownik* p_Sterownik) {
    __setItsSterownik(p_Sterownik);
}

void Panel_Klimatyzacji::_clearItsSterownik() {
    NOTIFY_RELATION_CLEARED("itsSterownik");
    itsSterownik = NULL;
}

#ifdef _OMINSTRUMENT
//#[ ignore
void OMAnimatedPanel_Klimatyzacji::serializeAttributes(AOMSAttributes* aomsAttributes) const {
    OMAnimatedModul::serializeAttributes(aomsAttributes);
}

void OMAnimatedPanel_Klimatyzacji::serializeRelations(AOMSRelations* aomsRelations) const {
    aomsRelations->addRelation("itsSterownik", false, true);
    if(myReal->itsSterownik)
        {
            aomsRelations->ADD_ITEM(myReal->itsSterownik);
        }
    OMAnimatedModul::serializeRelations(aomsRelations);
}
//#]

IMPLEMENT_META_S_P(Panel_Klimatyzacji, Default, false, Modul, OMAnimatedModul, OMAnimatedPanel_Klimatyzacji)

OMINIT_SUPERCLASS(Modul, OMAnimatedModul)

OMREGISTER_CLASS

IMPLEMENT_META_P(Panel_Klimatyzacji::Przycisk_Zwieksz_Temp, Default, Default, false, OMAnimatedPrzycisk_Zwieksz_Temp)

IMPLEMENT_META_P(Panel_Klimatyzacji::Przycisk_Zwieksz_Nawiew, Default, Default, false, OMAnimatedPrzycisk_Zwieksz_Nawiew)

IMPLEMENT_META_P(Panel_Klimatyzacji::Przycisk_Zmniejsz_Temp, Default, Default, false, OMAnimatedPrzycisk_Zmniejsz_Temp)

IMPLEMENT_META_P(Panel_Klimatyzacji::Przycisk_Zmniejsz_Nawiew, Default, Default, false, OMAnimatedPrzycisk_Zmniejsz_Nawiew)

IMPLEMENT_REACTIVE_META_SIMPLE_P(Panel_Klimatyzacji::Przycisk_Wylacz, Default, Default, false, OMAnimatedPrzycisk_Wylacz)

IMPLEMENT_REACTIVE_META_SIMPLE_P(Panel_Klimatyzacji::Przycisk_Wlacz, Default, Default, false, OMAnimatedPrzycisk_Wlacz)

IMPLEMENT_META_P(Panel_Klimatyzacji::Przycisk_Obieg_Zewnetrzny, Default, Default, false, OMAnimatedPrzycisk_Obieg_Zewnetrzny)

IMPLEMENT_META_P(Panel_Klimatyzacji::Przycisk_Obieg_Wewnetrzny, Default, Default, false, OMAnimatedPrzycisk_Obieg_Wewnetrzny)

IMPLEMENT_META_P(Panel_Klimatyzacji::Przycisk_Nadmuch_Srodkowy, Default, Default, false, OMAnimatedPrzycisk_Nadmuch_Srodkowy)

IMPLEMENT_META_P(Panel_Klimatyzacji::Przycisk_Nadmuch_Na_Szybe, Default, Default, false, OMAnimatedPrzycisk_Nadmuch_Na_Szybe)

IMPLEMENT_META_P(Panel_Klimatyzacji::Przycisk_Nadmuch_Na_Nogi, Default, Default, false, OMAnimatedPrzycisk_Nadmuch_Na_Nogi)
#endif // _OMINSTRUMENT

/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/Panel_Klimatyzacji.cpp
*********************************************************************/
